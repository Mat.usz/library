package pl.vobacom.szkolenie.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jboss.resteasy.annotations.providers.multipart.PartType;

import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserForm {

    @PartType(MediaType.TEXT_PLAIN)
    @FormParam("firstName")
    private String firstName;

    @PartType(MediaType.TEXT_PLAIN)
    @FormParam("lastName")
    private String lastName;
}
