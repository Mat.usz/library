package pl.vobacom.szkolenie.service;

import pl.vobacom.szkolenie.model.Book;
import pl.vobacom.szkolenie.model.User;
import pl.vobacom.szkolenie.repository.BookRepository;
import pl.vobacom.szkolenie.repository.UserRepository;
import pl.vobacom.szkolenie.dto.UserForm;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class BookService {

    @Inject
    private BookRepository bookRepository;

    @Inject
    private UserRepository userRepository;

    public List<Book> getAllTitles() {
        List<Book> books = bookRepository.getAvaiableBooks();
        List<Book> uniqueBooks = new ArrayList<>();
        for(Book book : books) {
            if (!uniqueBooks.contains(book)) {
                uniqueBooks.add(book);
            }
        }
        return uniqueBooks;
    }

    public Book getBookById(Long id) {
        return bookRepository.findById(id);
    }


    public void rentBook(UserForm userForm, Long bookId){
        User user = findOrCreateUser(userForm);
        Book book = bookRepository.findById(bookId);
        book.setUser(user);
    }

    public List<Book> getAllBooks() {
        return bookRepository.listAll();
    }

    public List<Book> getBooksByUserId(UserForm userForm) {
        User user = findOrCreateUser(userForm);
        return bookRepository.findBooksByUserId(user.getId());
    }

    private User findOrCreateUser(UserForm userForm) {
        User user = userRepository.findUser(userForm.getFirstName(), userForm.getLastName());
        if(user == null){
            user = new User(userForm.getFirstName(), userForm.getLastName());
            userRepository.persist(user);
        }
        return user;
    }

// id książki
    public void returnBook(Long id) {
        Book book = bookRepository.findById(id);
        book.setUser(null);
    }
}
