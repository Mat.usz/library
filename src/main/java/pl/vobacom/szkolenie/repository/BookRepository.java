package pl.vobacom.szkolenie.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import pl.vobacom.szkolenie.model.Book;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class BookRepository implements PanacheRepository<Book> {

    public List<Book> getAvaiableBooks(){
        return list("user_id = null");
    }

    public List<Book> findBooksByUserId(Long id) {
        return list("user_id", id);
    }
}
