package pl.vobacom.szkolenie.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import pl.vobacom.szkolenie.model.User;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UserRepository implements PanacheRepository<User> {

    public User findUser(String firstName, String lastName){
        return find("first_name = ?1 and last_name = ?2", firstName, lastName).firstResult();
    }
}
