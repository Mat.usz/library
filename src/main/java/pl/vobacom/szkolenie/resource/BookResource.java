package pl.vobacom.szkolenie.resource;

import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import pl.vobacom.szkolenie.dto.UserForm;
import pl.vobacom.szkolenie.model.Book;
import pl.vobacom.szkolenie.model.User;
import pl.vobacom.szkolenie.service.BookService;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.List;

@Path("/")
public class BookResource {

    @Inject
    private Template books;

    @Inject
    private Template userData;

    @Inject
    private Template library;

    @Inject
    private BookService bookService;

    @Inject
    private Template login;

    @Inject
    private Template myBooks;



    @GET
    @Produces(MediaType.TEXT_HTML)
    public TemplateInstance getUniqueTitles() {
        List<Book> allBooks = bookService.getAllTitles();
        return books.data("allBooks", allBooks);
    }

    @Path("rent")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public TemplateInstance getUserData(@QueryParam("id") Long id) {
        Book book = bookService.getBookById(id);
        return userData.data("book", book);
    }

    @Path("library")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public TemplateInstance getLibrary() {
        List<Book> allBooks = bookService.getAllBooks();
        return library.data("allBooks", allBooks);
    }

    @Path("rent")
    @POST
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Transactional
    public Response rentBook(@MultipartForm UserForm userForm, @QueryParam("id") Long id) {
        bookService.rentBook(userForm, id);
        return Response.seeOther(UriBuilder.fromPath("http://localhost:8080/").build()).build();
    }

    @Path("library/my-books")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public TemplateInstance logIn() {
        return login.instance();
    }

    @Path("library/my-books")
    @POST
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Transactional
    public TemplateInstance getUserBooks(@MultipartForm UserForm userForm) {
        List<Book> allBooks = bookService.getBooksByUserId(userForm);
        return myBooks.data("allBooks", allBooks);
    }

    @Path("return")
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Transactional
    public Response returnBook(@QueryParam("id") Long id) {
        bookService.returnBook(id);
        return Response.seeOther(UriBuilder.fromPath("http://localhost:8080/library/my-books").build()).build();
    }

    @Path("api/library")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLibraryJSON() {
        List<Book> allBooks = bookService.getAllBooks();
        return Response.ok(allBooks).build();
    }
}