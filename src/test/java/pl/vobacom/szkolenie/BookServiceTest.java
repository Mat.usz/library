package pl.vobacom.szkolenie;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.vobacom.szkolenie.dto.UserForm;
import pl.vobacom.szkolenie.model.Book;
import pl.vobacom.szkolenie.model.User;
import pl.vobacom.szkolenie.repository.BookRepository;
import pl.vobacom.szkolenie.repository.UserRepository;
import pl.vobacom.szkolenie.service.BookService;

import java.time.LocalDate;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class BookServiceTest {

    @InjectMocks
    private BookService bookService;

    @Mock
    private BookRepository bookRepository;

    @Mock
    private UserRepository userRepository;

    @Test
    public void shouldGetUniqueTitles() {

        // GIVEN
        Book book1 = new Book("TestTitle", "TestAuthor", "TestIsbn", 19, LocalDate.now());
        Book book2 = new Book("TestTitle", "TestAuthor", "TestIsbn", 19, LocalDate.now());
        List<Book> books = List.of(book1, book2);
        BDDMockito.given(bookRepository.getAvaiableBooks()).willReturn(books);

        // WHEN
        List<Book> uniqueTitles = bookService.getAllTitles();

        // THEN
        Book book = uniqueTitles.get(0);
        Assertions.assertEquals(book1.getTitle(), book.getTitle());
        Assertions.assertEquals(book1.getAuthor(), book.getAuthor());
        Assertions.assertEquals(book1.getIsbn(), book.getIsbn());
        Assertions.assertEquals(book1.getPages(), book.getPages());
        Assertions.assertEquals(book1.getPublishedDate(), book.getPublishedDate());
        Assertions.assertEquals(1, uniqueTitles.size());
    }

    @Test
    public void shouldRentBook() {

        //GIVEN
        final String firstName = "Jan";
        final String lastName = "Kowalski";
        final Long bookId = 1l;
        final UserForm userForm = new UserForm(firstName, lastName);

        User user = new User(firstName, lastName);
        BDDMockito.given(userRepository.findUser(firstName, lastName)).willReturn(user);
        Book book1 = new Book("TestTitle", "TestAuthor", "TestIsbn", 19, LocalDate.now());
        BDDMockito.given(bookRepository.findById(bookId)).willReturn(book1);

        //WHEN
        bookService.rentBook(userForm, bookId);

        //THEN
        User userResult = book1.getUser();
        Assertions.assertEquals(userResult.getFirstName(), firstName);
        Assertions.assertEquals(userResult.getLastName(), lastName);
    }

    @Test
    public void shouldRentBookAndCreateUser() {

        //GIVEN
        final String firstName = "Jan";
        final String lastName = "Kowalski";
        final Long bookId = 1l;
        final UserForm userForm = new UserForm(firstName, lastName);

        User user = new User(firstName, lastName);
        BDDMockito.given(userRepository.findUser(firstName, lastName)).willReturn(null);
        Book book1 = new Book("TestTitle", "TestAuthor", "TestIsbn", 19, LocalDate.now());
        BDDMockito.given(bookRepository.findById(bookId)).willReturn(book1);

        //WHEN
        bookService.rentBook(userForm, bookId);

        //THEN
        User userResult = book1.getUser();
        Assertions.assertEquals(userResult.getFirstName(), firstName);
        Assertions.assertEquals(userResult.getLastName(), lastName);
        BDDMockito.then(userRepository).should().persist(Mockito.any(User.class));
    }

}