package pl.vobacom.szkolenie;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class BookResourceTest {

    private final String title1 = "W pustyni i puszczy";
    private final String title2 = "Krzyżacy";
    private final String author1 = "Sienkiewicz";
    private final String author2 = "Sienkiewicz";
    private final String isbn1 = "1252436y347634";
    private final String isbn2 = "1252436y3476343";
    private final int pages1 = 123;
    private final int pages2 = 123;

    @Test
    public void shouldGetAllBooksPages() {

        //GIVEN & WHEN
        Response response = RestAssured.given()
                .when()
                .get()
                .then()
                .log().all()
                .statusCode(200)
                .extract().response();

        // THEN
        String html = response.htmlPath().prettyPrint();
        assertThat(html)
                .contains(title2)
                .contains(author2)
                .contains(isbn2)
                .contains(Integer.toString(pages2));
    }

    @Test
    public void shouldGetLibrary() {

        //GIVEN & WHEN
        Response response = RestAssured.given()
                .when()
                .get("library")
                .then()
                .log().all()
                .statusCode(200)
                .extract().response();

        // THEN
        String html = response.htmlPath().prettyPrint();
        Pattern pattern = Pattern.compile("card card--margin");
        Matcher matcher = pattern.matcher(html);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        assertThat(html)
                .contains(title1)
                .contains(title2)
                .contains(author1)
                .contains(author2)
                .contains(isbn1)
                .contains(isbn2)
                .contains(Integer.toString(pages1))
                .contains(Integer.toString(pages2));

        assertThat(count).isEqualTo(2);
    }
}
