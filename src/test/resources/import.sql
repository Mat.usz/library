INSERT INTO user (id, first_name, last_name) VALUES (1, 'Andrzej', 'Brzęczyszczykiewicz');
INSERT INTO book (id, title, author, isbn, pages, published_date, user_id) VALUES (1, 'W pustyni i puszczy', 'Sienkiewicz', '1252436y347634', '123', CURRENT_TIMESTAMP, 1);
INSERT INTO book (id, title, author, isbn, pages, published_date, user_id) VALUES (2, 'Krzyżacy', 'Sienkiewicz', '1252436y3476343', '123', CURRENT_TIMESTAMP, null);
